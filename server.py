import socket
import struct
Host = '127.0.0.1'
Port = 2012
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((Host, Port))
s.listen(1)
conn, addr = s.accept()
print (('Connected by'), addr)
while 1:
	data = conn.recv(2048)
	if not data: break
	print (('received data: '), data)
	conn.send(data)
conn.close()
